
module.exports.ReError = function (res, err, code) {
  if (typeof err !== "object" && typeof err.message != "undefined") {
    err = JSON.parse(err.message);
  }
  if (typeof code !== "undefined") res.statusCode = code;
  return res.json({ status: false, error: err, status_code: res.statusCode });
};

module.exports.ReSuccess = function (res, data, total, code) {
  let send_data = { status: true };
  if (typeof data == "object") {
    send_data = Object.assign(data, send_data);
  }
  if (typeof code !== "undefined") res.statusCode = code;
  if (typeof total == "number") {
    send_data = Object.assign({ total: total }, send_data);
  }
  send_data = Object.assign({ status_code: res.statusCode }, send_data);
  return res.json(send_data);
};
