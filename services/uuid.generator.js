const { v4: uuidv4 } = require('uuid');

async function getUid(model, column) {
    let uuid = uuidv4();
    let filter = {}
    filter[column] = uuid;
    let check_data = model.findOne({where : filter});
    if(check_data.id){
        await getUid(model, column);
    }else{
        return uuid;
    }
 };
 
 exports.getUid = getUid;
