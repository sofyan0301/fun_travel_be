const Joi = require('joi');

exports.validate = (data, schema) => {
    var message = {};
    let error = []
    Joi.validate(data, schema, { abortEarly: false }, (err, value) => {
        if (err) {
            for (let i = 0; i < err.details.length; i++) {
                var key = err.details[i].path;
                var val = err.details[i].message.replace(/([.*+?^=!:${}()_|\[\]\/\\])/gi, " ");
                message[key] = val.replace(/(["|\[\]\/\\])/gi, "");
            }
        }
    });
    if(Object.getOwnPropertyNames(message).length){
        error.push(message);
    }
    return error;
};
