const jwt = require("jsonwebtoken");
const { ReError, ReSuccess } = require("../services/util.service");
const BaseJoi = require('joi');
const Extension = require('joi-date-extensions');
const Joi = BaseJoi.extend(Extension);
const { validate } = require("../services/validator");
const Answers = require("../models").Answers;
const DetailAnswers = require("../models").DetailAnswers;
const Questions = require("../models").Questions;
const Cities = require("../models").Cities;
const Users = require("../models").Users;

const get = function (req, res) {
  Answers.findAll({
    include: [
      Cities,
      {
        model: DetailAnswers,
        as: "detailAnswers",
        include: [Questions],
      },
    ],
  })
    .then((result) => {
      let data_list = [];
      for (const data of result) {
        let data_detail = [];
        for (const detail of data.detailAnswers) {
          let dt = {
            qustion: detail.Question.name,
            answer: detail.name,
          };
          data_detail.push(dt);
        }
        let array_date_start = data.date_start.split("-");
        let array_date_end = data.date_start.split("-");
        let dt = {
          id: data.id,
          name: data.name,
          email: data.email,
          phone_number: data.phone_number,
          city_to: data.City.name,
          date_start: array_date_start[2] + "/" + array_date_start[1] + "/" + array_date_start[0],
          date_end: array_date_end[2] + "/" + array_date_end[1] + "/" + array_date_end[0],
          detail_answer: data_detail,
        };
        data_list.push(dt);
      }
      return ReSuccess(res, { data_list: data_list }, data_list.count, 200);
    })
    .catch((error) => {
      console.log(error);
      return ReError(res, { server: "internal server error." }, 401);
    });
};
module.exports.get = get;

const create = async function (req, res) {
  let schema = Joi.object().keys({
    name: Joi.string().required(),
    email: Joi.string().email({ minDomainAtoms: 2 }).required(),
    phone_number: Joi.string().required(),
    city_id: Joi.number().required(),
    date_start: Joi.date().format("YYYY-MM-DD").required(),
    date_end: Joi.date().format("YYYY-MM-DD").required(),
    answers: Joi.array()
      .items(
        Joi.object().keys({
          question_id: Joi.number().required(),
          answer_question: Joi.string().required(),
        })
      )
      .required(),
  });

  let validator = validate(req.body, schema);
  if (validator.length) {
    return ReError(res, validator, 401);
  } else {
    let answer_post = {
      name: req.body.name,
      email: req.body.email,
      phone_number: req.body.phone_number,
      city_id: req.body.city_id,
      date_start: req.body.date_start,
      date_end: req.body.date_end,
    };

    Answers.create(answer_post)
      .then(async(answers) => {
        let data_answers = req.body.answers
        let save_detail = await saveAnswer(req.body.answers, answers.id);
        if(data_answers.length && save_detail){
            return ReSuccess(res, { data_list: [] }, 1, 200);
        }else{
            Answers.destroy({where:{id : answers.id}})
            return ReSuccess(res, { data_list: [] }, 0, 200);
        }
      })
      .catch((error) => {
        console.log(error);
        return ReError(res, { server: "internal server error." }, 401);
      });
  }
};
module.exports.create = create;

saveAnswer = async (data_answers, answer_id) => {
    let success = true;
    for(const data_answer of data_answers){
        let result = await DetailAnswers.create({
            answer_id : answer_id,
            name : data_answer.answer_question,
            question_id : data_answer.question_id
        });
        if(!result){ success = false; break; }
    }
    return success;
}

const destroy = function (req, res) {
  Answers.destroy({ where: { id: req.params.id } })
    .then(() => {
      return ReSuccess(res, { data_list: {} }, 1, 200);
    })
    .catch((error) => {
      return ReError(res, { server: "internal server error." }, 401);
    });
};
module.exports.destroy = destroy;
