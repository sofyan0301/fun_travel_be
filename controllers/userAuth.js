const jwt = require("jsonwebtoken");
const { ReError, ReSuccess } = require("../services/util.service");
const Joi = require("joi");
const { validate } = require("../services/validator");
const Users = require("../models").Users;
const { getUid } = require("../services/uuid.generator");

const login = function (req, res) {
  let schema = Joi.object().keys({
    username: Joi.string().required(),
    password: Joi.string().required(),
  });

  let validator = validate(req.body, schema);
  if (validator.length) {
    return ReError(
      res,
      validator,
      401
    );
  } else {
    Users.findOne({
      where: {
        username: req.body.username,
      },
    })
      .then((user) => {
        if (!user) {
          return ReError(
            res,
            { password: "username atau password incorrect." },
            401
          );
        }
        user.comparePassword(req.body.password, (err, isMatch) => {
          if (isMatch && !err) {
            var date = new Date();
            date.setDate(date.getDate() + 360);
            let time = date.getTime();
            var token = jwt.sign(
              JSON.parse(JSON.stringify(user)),
              "keysecret",
              {
                expiresIn: time,
              }
            );
            jwt.verify(token, "keysecret", function (err, data) {
              console.log("loginnn", data);
            });
            let respone = {
              id: user.id,
              first_name: user.first_name,
              last_name: user.last_name,
              email: user.email,
              user_level_id: user.user_level_id,
              token: token,
            };
            return ReSuccess(res, { data_list: respone }, 1, 200);
          } else {
            return ReError(
              res,
              { password: "username atau password incorrect." },
              401
            );
          }
        });
      })
      .catch((error) => res.status(400).send(error));
  }
};
module.exports.login = login;

const memberRegister = async function (req, res) {
  let schema = Joi.object().keys({
    first_name: Joi.string().required(),
    last_name: Joi.string().allow("", null),
    email: Joi.string().email({ minDomainAtoms: 2 }).required(),
    username: Joi.string().required(),
    password: Joi.string().required(),
    phone_number: Joi.string().allow("", null),
  });

  let validator = validate(req.body, schema);
  if (validator.length) {
    return ReError(res, validator, 401);
  } else {
    let data_post = req.body;
    let uuid = await getUid(Users, "id");
    data_post["id"] = uuid;
    data_post["user_level_id"] = 2;
    Users.create(data_post)
      .then((result) => {
        return ReSuccess(res, { data_list: [] }, 1, 200);
      })
      .catch((error) => {
        console.log(error);
        return ReError(res, { server: "internal server error." }, 401);
      });
  }
};
module.exports.memberRegister = memberRegister;

const member = async function (req, res) {
  let user = req.user;
  let respone = {
    id: req.user.id,
    first_name: req.user.first_name,
    last_name: req.user.last_name,
    email: req.user.email,
    username: req.user.username,
    user_level_id: req.user.user_level_id,
    phone_number: req.user.phone_number,
  }
  return ReSuccess(res, { data_list: respone }, 1, 200);
};
module.exports.member = member;