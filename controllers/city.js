const jwt = require("jsonwebtoken");
const { ReError, ReSuccess } = require("../services/util.service");
const Joi = require("joi");
const { validate } = require("../services/validator");
const Cities = require("../models").Cities;
const Provinces = require("../models").Provinces;

const get = function (req, res) {
  Cities.findAll({
      include : [Provinces]
  })
    .then((result) => {
      let data_list = [];
      for (const data of result) {
        let dt = {
          id: data.id,
          name: data.name,
          province_id : data.province_id,
          province : data.Province.name
        };
        data_list.push(dt);
      }
      return ReSuccess(res, { data_list: data_list }, data_list.count, 200);
    })
    .catch((error) => {
      console.log(error);
      return ReError(res, { server: "internal server error." }, 401);
    });
};
module.exports.get = get;

const create = async function (req, res) {
  let schema = Joi.object().keys({
    name: Joi.string().required(),
    province_id: Joi.number().required(),
  });

  let validator = validate(req.body, schema);
  if (validator.length) {
    return ReError(res, validator, 401);
  } else {
    let data_post = req.body;
    Cities.create(data_post)
      .then((result) => {
        return ReSuccess(res, { data_list: [] }, 1, 200);
      })
      .catch((error) => {
        console.log(error);
        return ReError(res, { server: "internal server error." }, 401);
      });
  }
};
module.exports.create = create;

const update = function (req, res) {
  let schema = Joi.object().keys({
    name: Joi.string().required(),
    province_id: Joi.number().required(),
  });

  let validator = validate(req.body, schema);
  if (validator.length) {
    return ReError(res, validator, 401);
  } else {
    let data = req.body;
    Cities.update(data, { where: { id: req.params.id } })
      .then((result) => {
        return ReSuccess(res, { data_list: [] }, 1, 200);
      })
      .catch((error) => {
        console.log(error);
        return ReError(res, { server: "internal server error." }, 401);
      });
  }
};
module.exports.update = update;

const destroy = function (req, res) {
  Cities.destroy({ where: { id: req.params.id } })
    .then(() => {
      return ReSuccess(res, { data_list: {} }, 1, 200);
    })
    .catch((error) => {
        return ReError(res, { server: "internal server error." }, 401);
    });
};
module.exports.destroy = destroy;
