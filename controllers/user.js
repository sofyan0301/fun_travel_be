const jwt = require("jsonwebtoken");
const { ReError, ReSuccess } = require("../services/util.service");
const Joi = require("joi");
const { validate } = require("../services/validator");
const { getUid } = require("../services/uuid.generator");
const Users = require("../models").Users;
const UserLevels = require("../models").UserLevels;
var bcrypt = require("bcrypt-nodejs");

const get = function (req, res) {
  Users.findAll({
    include: [UserLevels],
  })
    .then((result) => {
      let data_list = [];
      for (const data of result) {
        let dt = {
          id: data.id,
          first_name: data.first_name,
          last_name: data.last_name,
          email: data.email,
          phone_number: data.phone_number,
          user_level_id: data.user_level_id,
          user_level: data.UserLevel.name,
        };
        data_list.push(dt);
      }
      return ReSuccess(res, { data_list: data_list }, data_list.count, 200);
    })
    .catch((error) => {
      console.log(error);
      return ReError(res, { server: "internal server error." }, 401);
    });
};
module.exports.get = get;

const getMember = function (req, res) {
  Users.findAll({
    where : {user_level_id : 2},
    include: [UserLevels],
  })
    .then((result) => {
      let data_list = [];
      for (const data of result) {
        let dt = {
          id: data.id,
          first_name: data.first_name,
          last_name: data.last_name,
          email: data.email,
          phone_number: data.phone_number,
          user_level_id: data.user_level_id,
          user_level: data.UserLevel.name,
        };
        data_list.push(dt);
      }
      return ReSuccess(res, { data_list: data_list }, data_list.count, 200);
    })
    .catch((error) => {
      console.log(error);
      return ReError(res, { server: "internal server error." }, 401);
    });
};
module.exports.getMember = getMember;

const create = async function (req, res) {
  let schema = Joi.object().keys({
    first_name: Joi.string().required(),
    last_name: Joi.string().allow("", null),
    email: Joi.string().email({ minDomainAtoms: 2 }).required(),
    username: Joi.string().required(),
    password: Joi.string().required(),
    phone_number: Joi.string().allow("", null),
    user_level_id: Joi.number().required(),
  });

  let validator = validate(req.body, schema);
  if (validator.length) {
    return ReError(res, validator, 401);
  } else {
    let data_post = req.body;
    let uuid = await getUid(Users, "id");
    data_post["id"] = uuid;
    Users.create(data_post)
      .then((result) => {
        return ReSuccess(res, { data_list: [] }, 1, 200);
      })
      .catch((error) => {
        console.log(error);
        return ReError(res, { server: "internal server error." }, 401);
      });
  }
};
module.exports.create = create;

const update = function (req, res) {
  let schema = Joi.object().keys({
    first_name: Joi.string().required(),
    last_name: Joi.string().allow("", null),
    email: Joi.string().email({ minDomainAtoms: 2 }).required(),
    username: Joi.string().required(),
    password: Joi.string(),
    phone_number: Joi.string().allow("", null),
    user_level_id: Joi.number().required(),
  });

  let validator = validate(req.body, schema);
  if (validator.length) {
    return ReError(res, validator, 401);
  } else {
    let data = req.body;
    if (req.body.password) {
      data.password = bcrypt.hashSync(
        data.password,
        bcrypt.genSaltSync(10),
        null
      );
    }
    Users.update(data, { where: { id: req.params.id } })
      .then((result) => {
        return ReSuccess(res, { data_list: [] }, 1, 200);
      })
      .catch((error) => {
        console.log(error);
        return ReError(res, { server: "internal server error." }, 401);
      });
  }
};
module.exports.update = update;

const destroy = function (req, res) {
  Users.destroy({ where: { id: req.params.id } })
    .then(() => {
      return ReSuccess(res, { data_list: {} }, 1, 200);
    })
    .catch((error) => {
        return ReError(res, { server: "internal server error." }, 401);
    });
};
module.exports.destroy = destroy;
