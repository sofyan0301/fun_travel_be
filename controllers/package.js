const jwt = require("jsonwebtoken");
const { ReError, ReSuccess } = require("../services/util.service");
const Joi = require("joi");
const { getUid } = require("../services/uuid.generator");
const { validate } = require("../services/validator");
const TourPackages = require("../models").TourPackages;
const Cities = require("../models").Cities;

const get = function (req, res) {
  TourPackages.findAll({
    include: [Cities],
  })
    .then((result) => {
      let data_list = [];
      for (const data of result) {
        let dt = {
          id: data.id,
          title: data.title,
          description: data.description,
          price: data.price,
          city_id: data.city_id,
          city: data.City.name,
          is_publish: data.is_publish,
        };
        data_list.push(dt);
      }
      return ReSuccess(res, { data_list: data_list }, data_list.count, 200);
    })
    .catch((error) => {
      console.log(error);
      return ReError(res, { server: "internal server error." }, 401);
    });
};
module.exports.get = get;

const create = async function (req, res) {
  let schema = Joi.object().keys({
    title: Joi.string().required(),
    description: Joi.string().required(),
    price: Joi.string().required(),
    city_id: Joi.number().required(),
    is_publish: Joi.string().valid("0", "1").required(),
  });

  let validator = validate(req.body, schema);
  if (validator.length) {
    return ReError(res, validator, 401);
  } else {
    let data_post = req.body;
    let uuid = await getUid(TourPackages, "id");
    data_post["id"] = uuid;
    TourPackages.create(data_post)
      .then((result) => {
        return ReSuccess(res, { data_list: [] }, 1, 200);
      })
      .catch((error) => {
        console.log(error);
        return ReError(res, { server: "internal server error." }, 401);
      });
  }
};
module.exports.create = create;

const update = function (req, res) {
  let schema = Joi.object().keys({
    title: Joi.string().required(),
    description: Joi.string().required(),
    price: Joi.string().required(),
    city_id: Joi.number().required(),
    is_publish: Joi.string().valid("0", "1").required(),
  });

  let validator = validate(req.body, schema);
  if (validator.length) {
    return ReError(res, validator, 401);
  } else {
    let data = req.body;
    TourPackages.update(data, { where: { id: req.params.id } })
      .then((result) => {
        return ReSuccess(res, { data_list: [] }, 1, 200);
      })
      .catch((error) => {
        console.log(error);
        return ReError(res, { server: "internal server error." }, 401);
      });
  }
};
module.exports.update = update;

const destroy = function (req, res) {
  TourPackages.destroy({ where: { id: req.params.id } })
    .then(() => {
      return ReSuccess(res, { data_list: {} }, 1, 200);
    })
    .catch((error) => {
      return ReError(res, { server: "internal server error." }, 401);
    });
};
module.exports.destroy = destroy;
