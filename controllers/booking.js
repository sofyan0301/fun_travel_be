const jwt = require("jsonwebtoken");
const { ReError, ReSuccess } = require("../services/util.service");
const Joi = require("joi");
const { validate } = require("../services/validator");
const { getUid } = require("../services/uuid.generator");
const Bookings = require("../models").Bookings;
const TourPackages = require("../models").TourPackages;
const Users = require("../models").Users;
const Cities = require("../models").Cities;

const get = function (req, res) {
  Bookings.findAll({
      include : [Users, {model : TourPackages, include : [Cities]}]
  })
    .then((result) => {
      let data_list = [];
      for (const data of result) {
        let dt = {
          booking_id: data.id,
          tour_package_id: data.TourPackage.id,
          tour_package_title: data.TourPackage.title,
          tour_package_description: data.TourPackage.description,
          tour_package_price: data.TourPackage.price,
          tour_package_city: data.TourPackage.City.name,
          member_name : data.User.first_name + "" + (data.User.last_name ? " "+data.User.last_name : ""),
          member_email : data.User.email,
          member_phone_number : data.User.phone_number,
          booking_date: data.createdAt.toDateString("dd MM YYYY H:I") + " " + data.createdAt.toLocaleTimeString('en-US'),
        };
        data_list.push(dt);
      }
      return ReSuccess(res, { data_list: data_list }, data_list.count, 200);
    })
    .catch((error) => {
      console.log(error);
      return ReError(res, { server: "internal server error." }, 401);
    });
};
module.exports.get = get;

const create = async function (req, res) {
  let schema = Joi.object().keys({
    user_id: Joi.string().required(),
    tour_package_id: Joi.string().required()
  });

  let validator = validate(req.body, schema);
  if (validator.length) {
    return ReError(res, validator, 401);
  } else {
    let data_post = req.body;
    let uuid = await getUid(Bookings, "id");
    data_post["id"] = uuid;
    Bookings.create(data_post)
      .then((result) => {
        return ReSuccess(res, { data_list: [] }, 1, 200);
      })
      .catch((error) => {
        console.log(error);
        return ReError(res, { server: "internal server error." }, 401);
      });
  }
};
module.exports.create = create;

const update = function (req, res) {
  let schema = Joi.object().keys({
    name: Joi.string().required()
  });

  let validator = validate(req.body, schema);
  if (validator.length) {
    return ReError(res, validator, 401);
  } else {
    let data = req.body;
    Bookings.update(data, { where: { id: req.params.id } })
      .then((result) => {
        return ReSuccess(res, { data_list: [] }, 1, 200);
      })
      .catch((error) => {
        console.log(error);
        return ReError(res, { server: "internal server error." }, 401);
      });
  }
};
module.exports.update = update;

const destroy = function (req, res) {
  Bookings.destroy({ where: { id: req.params.id } })
    .then(() => {
      return ReSuccess(res, { data_list: {} }, 1, 200);
    })
    .catch((error) => {
        return ReError(res, { server: "internal server error." }, 401);
    });
};
module.exports.destroy = destroy;
