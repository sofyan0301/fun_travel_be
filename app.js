var express = require('express');
var path = require("path");
var bodyParser = require('body-parser');
var logger = require('morgan');
var cors = require('cors')
var apiRouter = require('./routes/api/v1');
// var indexRouter = require('./routes/index');

var app = express();
app.use(cors())
app.use(bodyParser.json({limit:'50mb'})); 
app.use(bodyParser.urlencoded({extended:true, limit:'50mb'}));

app.use(logger('dev'));
app.use(express.json());
app.use('/v1', apiRouter);

app.use('/', function(req, res){
	res.statusCode = 200;//send the appropriate status code
	res.json({status:"success", message:"masuk pak eko", data:{}})
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});
module.exports = app;