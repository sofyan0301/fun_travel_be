/* jshint indent: 2 */
var bcrypt = require('bcrypt-nodejs');
module.exports = function(sequelize, DataTypes) {
  const Users = sequelize.define('Users', {
    id: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    first_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    last_name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    user_level_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    phone_number: {
      type: DataTypes.STRING,
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'Users'
  });
  Users.beforeSave((Users, options) => {
    if (Users.changed('password')) {
      Users.password = bcrypt.hashSync(Users.password, bcrypt.genSaltSync(10), null);
    }
  });
  
  Users.prototype.comparePassword = function (passw, cb) {
    bcrypt.compare(passw, this.password, function (err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
  };
  Users.associate = function(models) {
    Users.belongsTo(models.UserLevels, {foreignKey: 'user_level_id'});
  };
  return Users;
};
