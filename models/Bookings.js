/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const Bookings = sequelize.define('Bookings', {
    id: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    user_id: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    tour_package_id: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'Bookings'
  });

  Bookings.associate = function(models) {
    Bookings.belongsTo(models.Users, {foreignKey: 'user_id'});
    Bookings.belongsTo(models.TourPackages, {foreignKey: 'tour_package_id'});
  };
  return Bookings;
};
