/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const DetailAnswers = sequelize.define('DetailAnswers', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    answer_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    question_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'DetailAnswers'
  });

  DetailAnswers.associate = function (models) {
    DetailAnswers.belongsTo(models.Questions, { foreignKey: "question_id" });
    DetailAnswers.belongsTo(models.Answers, { foreignKey: "answer_id" });
  };

  return DetailAnswers;
};
