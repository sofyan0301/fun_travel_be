/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  const Answers = sequelize.define(
    "Answers",
    {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      phone_number: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      city_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      date_start: {
        type: DataTypes.DATEONLY,
        allowNull: false,
      },
      date_end: {
        type: DataTypes.DATEONLY,
        allowNull: false,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      tableName: "Answers",
    }
  );

  Answers.associate = function (models) {
    Answers.belongsTo(models.Cities, { foreignKey: "city_id" });
    Answers.hasMany(models.DetailAnswers, {
      as: "detailAnswers",
      foreignKey: "answer_id",
    });
  };

  return Answers;
};
