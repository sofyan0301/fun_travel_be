/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const TourPackages = sequelize.define('TourPackages', {
    id: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    price: {
      type: DataTypes.DOUBLE,
      allowNull: false
    },
    city_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    is_publish: {
      type: DataTypes.ENUM("0","1"),
      allowNull: true,
      defaultValue: '1'
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'TourPackages'
  });

  TourPackages.associate = function(models) {
    TourPackages.belongsTo(models.Cities, {foreignKey: 'city_id'});
  };

  return TourPackages;
};
