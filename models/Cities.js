/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const Cities = sequelize.define('Cities', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    province_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'Cities'
  });

  Cities.associate = function(models) {
    Cities.belongsTo(models.Provinces, {foreignKey: 'province_id'});
  };
  return Cities;
};
