var express = require("express");
var router = express.Router();
const passport = require("passport");
const querystring = require("querystring");
require("../../config/passport")(passport);
const { ReError, ReSuccess } = require("../../services/util.service");

const userAuthController = require('../../controllers/userAuth')
const userController = require('../../controllers/user');
const provinceController = require('../../controllers/province')
const cityController = require('../../controllers/city')
const questionController = require('../../controllers/question')
const answerController = require('../../controllers/answer')
const packageController = require('../../controllers/package')
const bookingController = require('../../controllers/booking')

router.post("/login", userAuthController.login);
router.post("/member-register", userAuthController.memberRegister);
router.get("/member", authenticateJwt, userAuthController.member);

//user
router.get("/user", authenticateJwt, userController.get);
router.get("/user-member", authenticateJwt, userController.getMember);
router.post("/user", authenticateJwt, userController.create);
router.put("/user/:id", authenticateJwt, userController.update);
router.delete("/user/:id", authenticateJwt, userController.destroy);

//province
router.get("/province", authenticateJwt, provinceController.get);
router.post("/province", authenticateJwt, provinceController.create);
router.put("/province/:id", authenticateJwt, provinceController.update);
router.delete("/province/:id", authenticateJwt, provinceController.destroy);

//city
router.get("/city", cityController.get);
router.post("/city", authenticateJwt, cityController.create);
router.put("/city/:id", authenticateJwt, cityController.update);
router.delete("/city/:id", authenticateJwt, cityController.destroy);

//question
router.get("/question", questionController.get);
router.post("/question", authenticateJwt, questionController.create);
router.put("/question/:id", authenticateJwt, questionController.update);
router.delete("/question/:id", authenticateJwt, questionController.destroy);

//answer
router.get("/answer", authenticateJwt, answerController.get);
router.post("/answer", answerController.create);
router.delete("/answer/:id", authenticateJwt, answerController.destroy);

//tour package
router.get("/tour-package", packageController.get);
router.post("/tour-package", authenticateJwt, packageController.create);
router.put("/tour-package/:id", authenticateJwt, packageController.update);
router.delete("/tour-package/:id", authenticateJwt, packageController.destroy);

//tour booking
router.get("/booking", authenticateJwt, bookingController.get);
router.post("/booking", bookingController.create);
router.delete("/booking/:id", authenticateJwt, bookingController.destroy);

function authenticateJwt(req, res, next) { 
    req.headers.authorization = "JWT "+req.headers.authorization;
    passport.authenticate('jwt', (error, user, info) => { 
      if(info){
        if (info.name === 'TokenExpiredError') info.status = 401;
        if (info.name === 'JsonWebTokenError') info.status = 401;
        if (info.name === 'Error') info.status = 401;
        
        if (error || !user) return ReError(res, info, 401);
      }
      
      req.user = user;
      next();
    })(req, res, next);
  }

module.exports = router;