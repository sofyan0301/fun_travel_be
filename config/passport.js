const JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;

const Users = require('../models').Users;

module.exports = function(passport) {
  const opts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('JWT'),
    secretOrKey: 'keysecret',
  };
  passport.use('jwt', new JwtStrategy(opts, function(jwt_payload, done) {
  //   if ( is_jwt_blacklisted(jwt_payload) ) {
  //     return done(err, false);
  // }else{
    Users
      .findByPk(jwt_payload.id)
      .then((user) => { return done(null, user); })
      .catch((error) => { return done(error, false); });
  // }
  }));

};
